<?php

/**
 * Implements hook_menu().
 */
function paypal_reports_menu() {
  $items = array();

  $items['admin/reports/paypal-reports'] = array(
    'title' => 'PayPal Reports',
    'access arguments' => array('access paypal_reports reports'),
    'page callback' => 'paypal_reports_instant_page',
    'file' => 'paypal_reports.reports.inc',
  );

  $items['admin/reports/paypal-reports/instant'] = array(
    'title' => 'Instant',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 0,
  );

  $items['admin/reports/paypal-reports/subscriptions'] = array(
    'title' => 'Subscription',
    'access arguments' => array('access paypal_reports reports'),
    'page callback' => 'paypal_reports_subscriptions_page',
    'file' => 'paypal_reports.reports.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
  );

  return $items;
}

/**
 * Add instant transaction.
 *
 * @param $transaction
 * @throws Exception
 */
function paypal_reports_add_instant_transaction($transaction) {
  db_insert('paypal_reports_instant')
    ->fields(array(
      'txn_id' => (string) $transaction['txn_id'],
      'item_name' => (string) $transaction['item_name'],
      'mc_currency' => (string) $transaction['mc_currency'],
      'mc_gross' => (float) $transaction['mc_gross'],
      'mc_fee' => (float) $transaction['mc_fee'],
      'payer_email' => (string) $transaction['payer_email'],
      'residence_country' => (string) $transaction['residence_country'],
      'payment_date' => strtotime($transaction['payment_date']),
      'data' => serialize($transaction),
    ))
    ->execute();
}

/**
 * Add subscription.
 *
 * @param $transaction
 * @throws Exception
 */
function paypal_reports_add_subscription($transaction) {
  db_insert('paypal_reports_subscription')
    ->fields(array(
      'subscr_id' => (string) $transaction['subscr_id'],
      'item_name' => (string) $transaction['item_name'],
      'mc_currency' => (string) $transaction['mc_currency'],
      'mc_amount1' => (float) $transaction['mc_amount1'],
      'mc_amount2' => (float) $transaction['mc_amount2'],
      'mc_amount3' => (float) $transaction['mc_amount3'],
      'period1' => (string) $transaction['period1'],
      'period2' => (string) $transaction['period2'],
      'period3' => (string) $transaction['period3'],
      'payer_email' => (string) $transaction['payer_email'],
      'residence_country' => (string) $transaction['residence_country'],
      'subscr_date' => strtotime($transaction['subscr_date']),
      'data' => serialize($transaction),
    ))
    ->execute();
}

function paypal_reports_cancel_subscription($transaction) {
  db_update('paypal_reports_subscription')
    ->fields(array('cancel_date' => time()))
    ->condition('subscr_id', $transaction['subscr_id'])
    ->execute();
}
